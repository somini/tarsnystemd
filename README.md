# tarsnystemd

SystemD and tarsnap, finally together.

## Features

- Un-opinionated, simply creates timer and service templates. The user is responsible for its usage
  - Doesn't work "out of the box", requires simple configuration. There's already `tarsnap` for ad-hoc backups
  - Do you want only manual backups? Run the service whenever you want
  - Do you want automated backups? Enable and configure the timers
- Pre-Backup scripts
  - See [examples](#examples)
- Works for both system-level and user-level backups.

## Usage

### Requirements

This requires `tarsnap` to be fully configured.
- You should have registered a tarsnap account, and it should have money on it
- There should be a keyfile generated using `tarsnap-keygen`
- There should be configuration file pointing to that keyfile, and a correct `cachedir`
  - System: `/etc/tarsnap/tarsnap.conf`
  - User: `$HOME/.tarsnaprc`

This is all mentioned on the documentation: https://www.tarsnap.com/gettingstarted.html

If this is not done, run as root:

```
# mkdir -p /var/cache/tarsnap
# tarsnap-keygen --keyfile /etc/tarsnap/main.key --machine "$(hostname)"
```

A good start for the (system) configuration file is available on [system](tarsnap-system.conf) or [user](tarsnap-user.conf) variants.

### Configuration

The configuration location is different between system and user-level backups.

Each configuration has a single required file, and other optional files:

- `${CONFIG}.conf`: A list of paths to backup. Mandatory.
  Passed to `tarsnap` with `-T`
- `${CONFIG}.exclude`: A list of paths to exclude from backup. Optional.
  Passed to `tarsnap` with `-X`
- `${CONFIG}.args`: Other arguments for `tarsnap`. Optional.
  This should be a newline separated file
- `${CONFIG}.pre`: Script to execute the pre-requisites. Optional.
  - Make sure this file is executable.
  - This starts on a runtime directory used to house backups (this is in RAM). Available as `$BACKUPS_RAM`
  - A cache directory (on disk) is also exists. Available as `$BACKUPS_DISK`
  - This is useful to create the files mentioned in `${CONFIG}.conf`.

#### System

This is when `tarsnystemd` runs as root

- Configuration Location: `/etc/tarsnap/tarsnystemd`

#### User

`$XDG_DATA_HOME` is usually `$HOME/.local/share`.

- Configuration Location: `$XDG_DATA_HOME/tarsnap/tarsnystemd`

### Running

To get a list of existing configurations, run:

```
tarsnystemd
```

To run a single backup, use:

```
tarsnystemd $CONFIG
```

or

```
systemctl [--user] start tarsnystemd@$CONFIG
```

The `--user` argument should be used for user-level backups.


To run a backup per day (the default timer configuration), run:

```
systemctl enable --now tarsnystemd@$CONFIG.timer
```

NOTE: user-level timers only work if the user is logged in (I guess). This is not reliable.

To configure the backup time, add an override:

```
systemctl [--user] edit tarsnystemd@$CONFIG.timer
```

```
[Timer]
# Empty `OnCalendar` to clear the daily backup
# Skip if if you want more backups
OnCalendar=
OnCalendar=hh:mm:ss
```

See the `systemd.timer(5)` manpage for timer configuration.

### Installing

This is available on AUR: https://aur.archlinux.org/packages/tarsnystemd/

### Examples

Some examples of best practices

#### Backup a git repository

Create a bundle file and backup that. See the [git
bundle](https://git-scm.com/docs/git-bundle) docs.

Here's an example to backup the repository in `/git/location`

On `$BACKUP.pre`, use:

```bash
bundle="$BACKUPS_RAM/BACKUP.bundle" # If the backup fits in RAM
cd /git/location
git bundle create "$bundle" --all
```

As `$BACKUP.conf`, use:

```
/run/tarsnystemd/BACKUP.bundle
```

Adjust the bundle paths if the repository to backup is too big for RAM, use `$BACKUPS_DISK`.

#### Backup etckeeper repository

The repository created by `etckeeper` is a special case of the git repository mentioned above.

There's no need to enable the SystemD timer for this, use a post-commit hook.

The contents of `/etc/.git/hooks/post-commit` (assuming you configured the backup as `etc`) are:

```bash
#!/bin/sh
exec systemctl start --no-block tarsnystemd@etc
```

An alternative to the `BACKUP.pre` file is the following file:

```bash
#!/bin/sh
exec etckeeper vcs bundle create "$BACKUPS_RAM/etc.bundle" --all
```

#### Backup a PostgreSQL database

Create a dump of the database contents, and backup that. Refer to the [backup
and restore](https://www.postgresql.org/docs/current/backup-dump.html)
documentation. This will show how to backup all the databases in the instance.

On `$BACKUP.pre`, use:

```bash
#!/bin/sh
su postgres sh -c "pg_dumpall --verbose --clean --file $BACKUPS_DISK/BACKUP.sql"
```

As `$BACKUP.conf`, use:

```
/var/cache/tarsnystemd/BACKUP.sql
```
